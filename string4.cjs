function string4(inventory) {
    let name = Object.entries(inventory).reduce((accumulator, currentValue) => {
        let change = accumulator + currentValue + " ";
        let string = change;
        return string;

    }, " ");
    let fullName = name.toLowerCase().replace(/\b\w/g, (convertString) => {
        return convertString.toUpperCase();
    });
    return fullName;
}
module.exports = string4
