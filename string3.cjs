function string3(date, monthNames) {
    let arr = date.split("/")
    for (let i = 0; i < monthNames.length; i++) {
        if (i == arr[1] - 1) {
            return monthNames[i];
        }
    }
}
module.exports = string3
