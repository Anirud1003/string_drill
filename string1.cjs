function string1(inventory) {
    let numbers = inventory.replaceAll(',', '').replaceAll("$", "");
    if (isNaN(numbers)) {
        return 0;
    }
    else {
        return parseFloat(numbers);
    }
}
module.exports = string1
